export default class MessagesService{
    getMessages(id) {
        const msg = JSON.parse(localStorage.getItem(id));

        if(msg){
            return msg
        }else{
            localStorage.setItem(id,JSON.stringify([]));
            return null
        }
    }

     setMessage(id,message) {
        const messages =  this.getMessages(id);

        if(messages){
            messages.push(message);
            localStorage.setItem(id,JSON.stringify(messages))
        }
    }

    getCurrentUser(){
        return JSON.parse(localStorage.getItem('currentUser'))
    }

    setCurrentUser(user){
        localStorage.setItem('currentUser',JSON.stringify(user))
    }
}

