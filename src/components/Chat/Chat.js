import React from 'react';
import './Chat.css'
import Message from "../Message";
import SendMessage from "../Send-message";
import MessagesService from "../../Services";

export default class Chat extends React.Component {
messagesService = new MessagesService();
user = this.props.user;
messages = this.messagesService.getMessages(this.user.u_token) || [];
    state = {
    messages:this.messages
};

updateMessages= (messages)=>{
    this.setState(() => {
        return {
            messages:messages
        }
    });
};

addItemData = (text) => {
    this.setState(({messages})=>{
        const updatedMessages = messages;
        const newId = updatedMessages.length ? updatedMessages[updatedMessages.length - 1].id + 1 : 1
        const currentUserToken = this.messagesService.getCurrentUser().u_token
        const dateOptions = {
                                weekday: 'long',
                                hour: 'numeric',
                                minute: 'numeric',
                            }
        const date  = new Date().toLocaleString("en-US",dateOptions)
        const item = { 
                       id: newId,
                       text, 
                       user_token:currentUserToken,
                       date
                    }
        this.messagesService.setMessage(this.user.u_token,item)
        updatedMessages.push(item)
        return {
            messages: updatedMessages
        }
    })
}
    render() {
        const {user} = this.props;
        return(
            <div className="col-md-8 col-xl-6 chat">
                <div className="card">
                    <div className="card-header msg_head">
                        <div className="d-flex bd-highlight">
                            <div className="img_cont">
                                <img
                                    src={user.image}
                                    className="rounded-circle user_img"/>
                                    <span className="online_icon"></span>
                            </div>
                            <div className="user_info">
                                <span>Chat with {user.name} {user.surname}</span>
                                <p>1767 Messages</p>
                            </div>
                        </div>
                    </div>
                    <div className="card-body msg_card_body">
                        <Message user={user} messages ={this.messages}/>
                    </div>
                    <div className="card-footer">
                        <SendMessage user={user} updateMessages={this.addItemData}/>
                    </div>
                </div>
            </div>
        )
    }
}