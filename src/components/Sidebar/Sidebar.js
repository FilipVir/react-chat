import React from 'react';
import './Sidebar.css'
import SidebarItem from "../Sidebar-item";

export default class Sidebar extends React.Component {
    contacts = this.props.contacts;

    render() {
        return(
            <div className="card mb-sm-3 mb-md-0 contacts_card">
                <div className="card-header">
                    <div className="input-group">
                        <input type="text" placeholder="Search..." name="" className="form-control search"/>
                            <div className="input-group-prepend">
                                <span className="input-group-text search_btn"><i className="fa fa-search"></i></span>
                            </div>
                    </div>
                </div>
                <div className="card-body contacts_body">
                    <ul className="contacts">
                        <SidebarItem contacts = {this.contacts} changeActiveDialog={this.props.changeActiveDialog}/>
                    </ul>
                </div>
            </div>
        )
    }
}