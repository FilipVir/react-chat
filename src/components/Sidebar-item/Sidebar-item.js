import React from "react";
import './Sidebar-item.css';
import {NavLink} from "react-router-dom";

function SidebarItem(props) {

    const drawContacts = () => {
        return props.contacts.map((user) => {
            return (
                <NavLink to={'/'+ user.id} activeClassName='active' key={user.id} onClick={()=>{setActive(user)}}>
                    <li>
                        <div className="d-flex bd-highlight">
                            <div className="img_cont">
                                <img
                                    src={user.image}
                                    className="rounded-circle user_img"/>
                            </div>
                            <div className="user_info">
                                <span>{user.name} {user.surname}</span>
                                <p>{user.name} is {user.status}</p>
                            </div>
                        </div>
                    </li>
                </NavLink>
            )
        })
    };

    const setActive = (user) => {
        const {changeActiveDialog} = props;

        changeActiveDialog(user)
    };

    return (
        <>
            {drawContacts()}
        </>
    );
}

export default SidebarItem;


