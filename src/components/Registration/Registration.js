import React from 'react';
import './Registration.css'

export default class Registration extends React.Component {
    state = {
        email: '',
        password: '',
        name: '',
        surname: '',
        emailValid: false,
        passValid: false,
        nameValid: false,
        surnameValid: false
    }


    render() {
        return (
            <div className="container pl-0  mt-2 ">
                <h1 className="d-flex justify-content-center mb-3">Sign Up</h1>
                <form>
                    <div className=" row">
                        <div className=" col">
                            <label htmlFor =" personName">Name</label>
                            <input type=" text" className=" form-control" placeholder=" Name"/>
                        </div>

                        <div className="col">
                            <label htmlFor ="personSurname">Surname</label>
                            <input type="text" className="form-control" formControlName="personSurname" placeholder="Surname"/>
                        </div>
                    </div>

                    <div className="row mt-4">
                        <div className="col">
                            <label htmlFor ="personEmail">Email</label>
                            <input type="email" className="form-control" placeholder="Email"/>
                        </div>
                        <div className="col">
                            <label htmlFor ="personPassword">Password</label>
                            <input type="password" className="form-control" id="personPassword" placeholder="Password"/>
                        </div>
                    </div>
                    <button type="submit" className="btn btn-primary mt-2">
                        Sign up
                    </button>
                </form>
            </div>
        )
    }
}