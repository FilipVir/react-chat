import React from 'react';
import './App.css';
import Sidebar from "../Sidebar/Sidebar";
import Chat from "../Chat/Chat";
import {BrowserRouter, Route} from "react-router-dom";
import MessagesService from "../../Services";
import Login from "../Login";
import Registration from "../Registration";

export default class App extends React.Component {
    contacts = [
        {
            id: 1,
            name: "Mike",
            surname: "Costa",
            status: 'online',
            image:'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS58hdNgOr3kQ90PX83Z9pnPz1WIm3brTQzDfEfUS7jjKpuwC2M',
            u_token: 'mikwksks'
        },
        {
            id: 2,
            name: "Linus",
            surname: "Costa",
            status: 'online',
            image:'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS58hdNgOr3kQ90PX83Z9pnPz1WIm3brTQzDfEfUS7jjKpuwC2M',
            u_token: 'linuwksks'
        },
        {
            id: 3,
            name: "Jon",
            surname: "Costa",
            status: 'online',
            image:'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS58hdNgOr3kQ90PX83Z9pnPz1WIm3brTQzDfEfUS7jjKpuwC2M',
            u_token: 'jonwksks'
        },
        {
            id: 4,
            name: "Peter",
            surname: "Costa",
            status: 'offline',
            image:'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS58hdNgOr3kQ90PX83Z9pnPz1WIm3brTQzDfEfUS7jjKpuwC2M',
            u_token: 'petwksks'
        },
        {
            id: 5,
            name: "Antony",
            surname: "Costa",
            status: 'offline',
            image:'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS58hdNgOr3kQ90PX83Z9pnPz1WIm3brTQzDfEfUS7jjKpuwC2M',
            u_token: 'antksks'
        }
    ];
    user =  {
        id: 45,
        name: "Bill",
        surname: "Cosby",
        status: 'offline',
        image:'https://media.istockphoto.com/photos/happy-laughing-man-picture-id544358212?k=6&m=544358212&s=612x612&w=0&h=odURMNz2hty8LIfpVahaaUKpGU4vd-UlZx4jy-OAnJA=',
        u_token: 'antksrfeks'
    }

    messages = new MessagesService().setCurrentUser(this.user);

    state={
      activeDialogId: 1,
      activeUser: this.contacts[0]
    };

    changeActiveDialog(user){
        this.setState(() => {
            return {
                activeDialogId: user.id,
                activeUser: user
            }
        });
        console.log(user);
    };

    render() {
        return (
            <BrowserRouter>
                <div className="container-fluid h-100">
                    <div className="row justify-content-center h-100">
                        <Login/>
                        <Registration/>
                        {/*<Sidebar contacts={this.contacts} changeActiveDialog ={(id)=>this.changeActiveDialog(id)}/>*/}
                        {/*<Route path={'/'+ this.state.activeDialogId} component={()=> <Chat user={this.state.activeUser}/>}/>*/}
                    </div>
                </div>
            </BrowserRouter>
        );
    }
}

