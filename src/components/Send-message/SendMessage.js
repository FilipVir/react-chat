import React from "react";

export default class SendMessage extends React.Component {
    state = {text:''};
  
sendMessage = (el) => {
    el.preventDefault();
   this.props.updateMessages(this.state.text)
};

inputChange = (e)=>{
    this.setState({
        text: e.target.value
    })
}

    render() {
        return (
                <form onSubmit={this.sendMessage} className="d-flex bd-highlight">
                    <div className="input-group">
                    <input name=""  onChange={this.inputChange} className="form-control type_msg " placeholder="Type your message..."/>
                        <button type='submit' className="input-group-text send_btn"><i
                            className="fa fa-paper-plane"></i></button>
                    </div>
                </form>
        );
    }
}