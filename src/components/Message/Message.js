import React from "react";
import MessagesService from "../../Services"

export default class Message extends React.Component {
    messagesService = new MessagesService();
    user = this.props.user;

    state = {
        user: this.user,
        messages: this.props.messages
    };

    getMessages() {
        const user = this.state.user;
        const messages = this.state.messages;
        const currentUser = this.messagesService.getCurrentUser();

        if (messages) {
           return  messages.map(message => {
                if (message.user_token === currentUser.u_token) {
                    return (
                        <div className="d-flex justify-content-end mb-4" key={message.id}>
                            <div className="msg_cotainer_send">
                                {message.text}
                                <span className="msg_time_send">{message.date}</span>
                            </div>
                            <div className="img_cont_msg">
                                <img
                                    src={currentUser.image}
                                    className="rounded-circle user_img_msg"/>
                            </div>
                        </div>
                    )
                } else {
                    return (
                        <div className="d-flex justify-content-start mb-4" key={message.id}>
                            <div className="img_cont_msg">
                                <img
                                    src={user.image}
                                    className="rounded-circle user_img_msg"/>
                            </div>
                            <div className="msg_cotainer">
                                {message.text}
                                <span className="msg_time">{message.date}</span>
                            </div>
                        </div>
                    )
                }
            })
        } else {
            return null
        }
    }

    render() {

        return (
            <>
                {this.getMessages()}
            </>
        );
    }
}